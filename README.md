Realizar uma pesquisa dos prós e contras de cada
tipo de servidor web disponível para as plataformas
GNU/linux.
Baseando-se nos mais utilizados e mais
acessíveis.
Criar um relatório executivo apresentando os
pontos fortes e os pontos fracos de cada opção.
Criar uma guia de recomendações sobre quais
especificações(hardware) o cliente deveria seguir para ter o
melhor custoXbenefício
Não esqueça das referências.

A baixo seuge resumo de alguns distros gnu/linux 

SUSE Linux Enterprise Server
The Micro Focus-owned (but independently operated) SUSE Linux Enterprise Server (SLES) is stable, easy to maintain and offers 24x7 rapid-response support for those who don't have the time or patience for lengthy troubleshooting calls. And the SUSE consulting teams will have you meeting your SLAs and making your accountants happy to boot.

 Similar to how Red Hat's RHEL is based on the open-source Fedora distribution, SLES is based on the open-source openSUSE Linux distro, with SLES focusing on stability and support over leading-edge features and technologies. 

The most recent major release, SUSE Linux Enterprise Server 12 (SLES 12), debuted in late October 2014 and introduced new features like framework for Docker, full system rollback, live kernel patching enablement and software modules for "increasing data center uptime, improving operational efficiency and accelerating the adoption of open source innovation," according to SUSE.

SLES 12 SP2 (Service Pack 2), the most recent update for SUSE, arrived in December 2016 with enterprise production support for Open vSwitch with DPDK (Data Plane Development Kit) and secure cryptoprocessor standard TPM (Trusted Platform Module) 2.0 support.

CentOS
If you operate a website through a web hosting company, there's a very good chance your web server is powered by CentOS Linux. This low-cost clone of Red Hat Enterprise Linux isn't strictly commercial, but since it's based on RHEL, you can leverage commercial support for it.

Short for Community Enterprise Operating System,  CentOS has largely operated as a community-driven project that used the RHEL code, removed all Red Hat's trademarks, and made the Linux server OS available for free use and distribution.

In 2014 the focus shifted following Red Hat and CentOS announcing they would collaborate going forward and that CentOS would serve to address the gap between the community-innovation-focused Fedora platform and the enterprise-grade, commercially-deployed Red Hat Enterprise Linux platform.

CentOS will continue to deliver a community-oriented operating system with a mission of helping users develop and adopt open source technologies on a Linux server distribution that is more consistent and conservative than Fedora's more innovative role.

At the same time, CentOS will remain free, with support provided by the community-led CentOS project rather than through Red Hat. CentOS released CentOS 7.2 in December 2015, which is derived from Red Hat Enterprise Linux 7.2.

Debian
If you're confused by Debian's inclusion here, don't be. Debian doesn't have formal commercial support but you can connect with Debian-savvy consultants around the world via their Consultants page. Debian originated in 1993 and has spawned more child distributions than any other parent Linux distribution, including Ubuntu, Linux Mint and Vyatta.

 Debian remains a popular option for those who value stability over the latest features. The latest major stable version of Debian, Debian 8 "jessie," was released in April 2015, and it will be supported for five years.

Debian 8 marks the switch to the systemd init system over the old SysVinit init system, and includes the latest releses of the Linux Kernel, Apache, LibreOffice, Perl, Python, Xen Hypervisor, GNU Compiler Collection and the GNOME and Xfce desktop environments.

Cronograma
26/7 - incio projeto bitbucket - servidores GNU/Linux
28/7 - pesquisa mais populares distros para servidor
31/7 - comparação efetiva para os mesmos 
2/8 - elaboração do relatorio
4/8 - finalização e apresentação.